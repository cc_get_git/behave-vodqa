# Running Tests From Behave


To run this example copy the git directory using ```git clone https://gitlab.com/cc_get_git/behave-vodqa.git```

Please do create a python virtual environment in this directory so that it doesn't mess your global py packages, this can be done by  
Windows : http://docs.python-guide.org/en/latest/starting/install3/win/
Windows python virtual environment : http://docs.python-guide.org/en/latest/dev/virtualenvs/#virtualenvironments-ref

OSX : http://docs.python-guide.org/en/latest/starting/install3/osx/
OSX python virtual environment : http://docs.python-guide.org/en/latest/dev/virtualenvs/#virtualenvironments-ref 

activate your python environment : Im using python3 default venv and the virtual env exists in ".venv" 
so i use ```source .venv/bin/activate``` 

and install the requirements by running ```pip install -r requirements-dev.txt```

And the go to the root directory of your folder and then do ```behave```

