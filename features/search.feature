Feature: Search

  @ninja
  Scenario: StackOverflow Automation
    Given I navigate to StackOverflow homepage
    And StackOverflow Logo is displayed
    When I add any tag to the URL
    Then page title contains the tag
    And Shows all questions tagged with it
    And Takes screenshot of the page
    And Goes to next page

  @samurai
  Scenario Outline: User searches KBQs
    Given I navigate to the PyPi Home page
    When I search for <search_term>
    Then I am taken to the PyPi Search Results page
    And I see a search result <search_result>

    Examples: KBQ Questions
      | search_term                        | search_result                |
      | behave                             |  behave 1.2.5                |
      | numpy                              |  numpy 1.14.1                |

